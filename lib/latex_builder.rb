# frozen_string_literal: true

require_relative 'latex_builder/version'
require_relative 'latex_builder/document'

module LatexBuilder
  class Error < StandardError; end
end
