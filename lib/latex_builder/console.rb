# frozen_string_literal: true

require 'optparse'
require 'ostruct'

require_relative 'parser'

module LatexBuilder
  class Console
    def run args
      _options, file = parse args

      document = Parser.parse file
      puts document.to_latex
    end

    def parse args # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      options = OpenStruct.new
      parser = OptionParser.new do |opts|
        opts.banner = "#{opts.program_name} [options] file"
        opts.version = LatexBuilder::VERSION

        opts.on '-v', '--[no-]verbose', 'Run verbosely' do |v|
          options.verbose = v
        end

        opts.separator ''
        opts.separator 'Common options:'

        opts.on_tail('-h', '--help', 'Show this message') do
          puts opts.help
          exit
        end

        opts.on_tail('--version', 'Show version') do
          puts "#{opts.program_name} - #{opts.version}"
          exit
        end
      end
      parser.parse! args

      parser.abort "File not specified\n#{parser.help}" if args.size.zero?
      parser.abort 'Multiple files specified' if args.size > 1

      file = args.first

      [options, file]
    end
  end
end
