# frozen_string_literal: true

require_relative 'nodes/command'
require_relative 'nodes/environment'
require_relative 'nodes/plain_latex'

module LatexBuilder
  class NodeParser
    attr_reader :nodes

    def initialize
      @nodes = []
      @insert_mode = :append
    end

    def latex_lines
      @nodes.inject([]) {|all, node| all + node.latex_lines }
    end

    def append &block
      with_insert_mode(:append) { instance_exec(&block) }
    end

    def prepend &block
      with_insert_mode(:prepend) { instance_exec(&block) }
    end

    def latex latex_content
      insert_node Nodes::PlainLatex.new(latex_content)
    end

    def respond_to_missing?(*)
      true
    end

    def method_missing name, *args, &block # rubocop:disable Style/MethodMissingSuper
      block.nil? ? command(name, *args) : environment(name, *args, &block)
    end

    def command name, *args
      insert_node Nodes::Command.new(name, *args)
    end

    def environment name, *args, &block
      insert_node Nodes::Environment.new(name, *args, &block)
    end

    def append_node node
      @nodes.insert(-1, node)
    end

    def prepend_node node
      @nodes.insert 0, node
    end

    def insert_node node
      case @insert_mode
      when :append then append_node node
      when :prepend then prepend_node node
      else; raise "Unknown insert mode #{@insert_mode.inspect}"
      end
    end

    private

    def with_insert_mode new_mode
      old_mode = @insert_mode
      @insert_mode = new_mode
      yield old_mode
      @insert_mode = old_mode
    end
  end
end
