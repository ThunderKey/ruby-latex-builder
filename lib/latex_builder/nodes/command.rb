# frozen_string_literal: true

require_relative '../string_extensions'
require_relative 'node'

module LatexBuilder::Nodes
  class Command < Node
    attr_reader :command, :args

    def initialize command, *args
      @command = command
      @args = args.map {|arg| arg.is_a?(String) ? arg.required : arg }
    end

    def latex_lines
      line = "\\#{command}"
      line += args.map(&:to_latex).join
      [line]
    end
  end
end
