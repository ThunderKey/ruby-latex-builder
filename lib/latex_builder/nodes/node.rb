# frozen_string_literal: true

module LatexBuilder::Nodes
  class Node
    def to_latex
      latex_lines.join "\n"
    end

    alias to_s to_latex
  end
end
