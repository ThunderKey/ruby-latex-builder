# frozen_string_literal: true

require_relative 'node'

module LatexBuilder::Nodes
  class PlainLatex < Node
    attr_reader :latex_lines

    def initialize content
      @latex_lines = content.is_a?(String) ? content.split("\n") : content
      @latex_lines.insert 0, '' unless @latex_lines.first.empty?
      @latex_lines.push '' unless @latex_lines.last.empty?
    end
  end
end
