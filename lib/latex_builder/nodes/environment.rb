# frozen_string_literal: true

require_relative 'plain_latex'
require_relative '../node_parser'
require_relative 'node'

module LatexBuilder::Nodes
  class Environment < Node
    attr_reader :name

    def initialize name, &block
      @name = name
      @node_parser = LatexBuilder::NodeParser.new
      append(&block) unless block.nil?
    end

    def append &block
      @node_parser.append(&block)
    end

    def prepend &block
      @node_parser.prepend(&block)
    end

    def latex_lines
      lines = ["\\begin{#{name}}"]
      lines += @node_parser.latex_lines.map {|l| "  #{l}".rstrip }
      lines << "\\end{#{name}}"
      lines
    end
  end
end
