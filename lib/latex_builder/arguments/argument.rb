# frozen_string_literal: true

module LatexBuilder::Arguments
  class Argument
    attr_reader :name

    def initialize name
      @name = name
    end
  end
end
