# frozen_string_literal: true

require_relative 'argument'

module LatexBuilder::Arguments
  class OptionalArgument < Argument
    def to_latex
      "[#{name}]"
    end
  end
end
