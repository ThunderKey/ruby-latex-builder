# frozen_string_literal: true

require_relative 'document'

module LatexBuilder
  class Parser
    def self.parse file
      new(file).parse
    end

    attr_reader :file

    def initialize file
      @file = File.absolute_path file
    end

    def parse
      content = File.read @file
      Document.new { binding.eval content } # rubocop:disable Security/Eval
    end
  end
end
