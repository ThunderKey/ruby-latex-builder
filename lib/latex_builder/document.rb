# frozen_string_literal: true

require_relative 'node_parser'
require_relative 'string_extensions'

module LatexBuilder
  class Document
    attr_writer :format, :type

    def initialize &block
      @type = 'report'
      @format = 'a4'
      @documentclass = nil
      @node_parser = NodeParser.new
      append(&block) unless block.nil?
    end

    def append &block
      instance_exec(&block)
    end

    def to_latex
      documentclass = @documentclass || Nodes::Command.new(:documentclass, @format.optional, @type)
      @node_parser.prepend_node documentclass
      @node_parser.latex_lines.join "\n"
    end

    def documentclass *args
      @documentclass = Command.new :documentclass, *args
    end

    def method_missing name, *args, &block
      return super unless @node_parser.respond_to? name

      @node_parser.send name, *args, &block
    end

    def respond_to_missing? name
      @node_parser.respond_to? name
    end
  end
end
