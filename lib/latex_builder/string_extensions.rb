# frozen_string_literal: true

require_relative 'arguments/optional_argument'
require_relative 'arguments/required_argument'

class String
  def optional
    LatexBuilder::Arguments::OptionalArgument.new self
  end

  def required
    LatexBuilder::Arguments::RequiredArgument.new self
  end
end
