# frozen_string_literal: true

RSpec.describe LatexBuilder::Console do
  subject(:console) { described_class.new }

  it 'parses a simple file correctly' do
    expect do
      console.run ['spec/examples/simple.rb']
    end.to output(<<-OUTPUT).to_stdout
\\documentclass[a4]{article}
\\begin{document}
  \\section{Test Section}
  \\subsection{Test Subsection}

  Test $x = y$

\\end{document}
OUTPUT
  end
end
