# frozen_string_literal: true

RSpec.describe LatexBuilder::Document do
  let(:document) { described_class.new }

  it 'compiles a simple latex file correctly' do # rubocop:disable RSpec/ExampleLength
    document.append do
      document do
        section 'My Section 1'

        latex 'My Test Content 1'

        section 'My Section 2'

        latex 'My Test Content 2'
      end
    end

    expect(document.to_latex).to eq <<-LATEX.rstrip
\\documentclass[a4]{report}
\\begin{document}
  \\section{My Section 1}

  My Test Content 1

  \\section{My Section 2}

  My Test Content 2

\\end{document}
LATEX
  end
end
