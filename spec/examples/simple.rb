# frozen_string_literal: true

self.type = 'article'

document do
  section 'Test Section'

  subsection 'Test Subsection'

  latex <<-LATEX
Test $x = y$
LATEX
end
